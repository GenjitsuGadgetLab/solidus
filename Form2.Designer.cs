﻿namespace Solidus
{
    partial class StateMapEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.btnFinish = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.nowInput = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.maxC = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.minC = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.maxT = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.minT = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbX = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lstPhases = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lstStruct = new System.Windows.Forms.ListBox();
            this.backgroundPicker = new IgorLib.FilePicker();
            this.chkBackground = new System.Windows.Forms.CheckBox();
            this.stateMapEditView1 = new Solidus.StateMapEditView();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minC)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minT)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnFinish,
            this.toolStripStatusLabel1,
            this.lblStatus,
            this.toolStripStatusLabel2,
            this.nowInput});
            this.statusStrip1.Location = new System.Drawing.Point(0, 774);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(878, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // btnFinish
            // 
            this.btnFinish.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnFinish.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(100, 19);
            this.btnFinish.Text = "Завершить ввод";
            this.btnFinish.Visible = false;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(4, 17);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(165, 17);
            this.lblStatus.Text = "Вводите при помощи мыши";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.toolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(4, 17);
            // 
            // nowInput
            // 
            this.nowInput.Name = "nowInput";
            this.nowInput.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.backgroundPicker);
            this.groupBox1.Controls.Add(this.chkBackground);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(709, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 774);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры";
            // 
            // tbName
            // 
            this.tbName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbName.Location = new System.Drawing.Point(9, 14);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(152, 21);
            this.tbName.TabIndex = 10;
            this.tbName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.maxC);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.minC);
            this.groupBox6.Location = new System.Drawing.Point(9, 118);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(152, 71);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Концентрация";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Максимум";
            // 
            // maxC
            // 
            this.maxC.DecimalPlaces = 5;
            this.maxC.Location = new System.Drawing.Point(74, 45);
            this.maxC.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.maxC.Name = "maxC";
            this.maxC.Size = new System.Drawing.Size(78, 20);
            this.maxC.TabIndex = 6;
            this.maxC.ValueChanged += new System.EventHandler(this.maxC_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Минимум";
            // 
            // minC
            // 
            this.minC.DecimalPlaces = 5;
            this.minC.Location = new System.Drawing.Point(74, 19);
            this.minC.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.minC.Name = "minC";
            this.minC.Size = new System.Drawing.Size(78, 20);
            this.minC.TabIndex = 4;
            this.minC.ValueChanged += new System.EventHandler(this.minC_ValueChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.maxT);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.minT);
            this.groupBox5.Location = new System.Drawing.Point(9, 41);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(152, 71);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Температура";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Максимум";
            // 
            // maxT
            // 
            this.maxT.Location = new System.Drawing.Point(74, 45);
            this.maxT.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.maxT.Name = "maxT";
            this.maxT.Size = new System.Drawing.Size(78, 20);
            this.maxT.TabIndex = 6;
            this.maxT.ValueChanged += new System.EventHandler(this.maxT_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Минимум";
            // 
            // minT
            // 
            this.minT.Location = new System.Drawing.Point(74, 19);
            this.minT.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.minT.Name = "minT";
            this.minT.Size = new System.Drawing.Size(78, 20);
            this.minT.TabIndex = 4;
            this.minT.ValueChanged += new System.EventHandler(this.minT_ValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.tbY);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.tbX);
            this.groupBox4.Location = new System.Drawing.Point(6, 675);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(157, 96);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Точка";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(7, 70);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(136, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Ввод";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "C";
            // 
            // tbY
            // 
            this.tbY.Location = new System.Drawing.Point(7, 46);
            this.tbY.Name = "tbY";
            this.tbY.Size = new System.Drawing.Size(115, 20);
            this.tbY.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(128, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "%";
            // 
            // tbX
            // 
            this.tbX.Location = new System.Drawing.Point(7, 20);
            this.tbX.Name = "tbX";
            this.tbX.Size = new System.Drawing.Size(115, 20);
            this.tbX.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.lstPhases);
            this.groupBox3.Location = new System.Drawing.Point(6, 257);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(157, 202);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Фазы";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(83, 170);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(68, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Удалить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(6, 170);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Добавить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lstPhases
            // 
            this.lstPhases.FormattingEnabled = true;
            this.lstPhases.Location = new System.Drawing.Point(6, 17);
            this.lstPhases.Name = "lstPhases";
            this.lstPhases.Size = new System.Drawing.Size(145, 147);
            this.lstPhases.TabIndex = 0;
            this.lstPhases.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstPhases_MouseClick);
            this.lstPhases.SelectedIndexChanged += new System.EventHandler(this.lstPhases_SelectedIndexChanged);
            this.lstPhases.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstPhases_MouseDoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDel);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.lstStruct);
            this.groupBox2.Location = new System.Drawing.Point(6, 465);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(157, 203);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Структуры";
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(83, 170);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(68, 23);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "Удалить";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 170);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lstStruct
            // 
            this.lstStruct.FormattingEnabled = true;
            this.lstStruct.Location = new System.Drawing.Point(6, 17);
            this.lstStruct.Name = "lstStruct";
            this.lstStruct.Size = new System.Drawing.Size(145, 147);
            this.lstStruct.TabIndex = 0;
            this.lstStruct.SelectedIndexChanged += new System.EventHandler(this.lstStruct_SelectedIndexChanged);
            this.lstStruct.DoubleClick += new System.EventHandler(this.lstStruct_DoubleClick);
            // 
            // backgroundPicker
            // 
            this.backgroundPicker.BrowseButtonCaption = "...";
            this.backgroundPicker.Filter = "*.jpg;*.png;*.jpeg;*.bmp|*.jpg;*.png;*.jpeg;*.bmp";
            this.backgroundPicker.Kind = IgorLib.FilePicker.FilePickerKind.FileOpen;
            this.backgroundPicker.Location = new System.Drawing.Point(6, 230);
            this.backgroundPicker.Name = "backgroundPicker";
            this.backgroundPicker.Root = null;
            this.backgroundPicker.SelectedPath = "";
            this.backgroundPicker.ShowFullPath = true;
            this.backgroundPicker.ShowNewFolder = false;
            this.backgroundPicker.Size = new System.Drawing.Size(155, 21);
            this.backgroundPicker.TabIndex = 4;
            this.backgroundPicker.Title = null;
            this.backgroundPicker.SelectionChanged += new IgorLib.FilePicker.SelectionChangedEventHandler(this.backgroundPicker_SelectionChanged);
            // 
            // chkBackground
            // 
            this.chkBackground.AutoSize = true;
            this.chkBackground.Location = new System.Drawing.Point(9, 207);
            this.chkBackground.Name = "chkBackground";
            this.chkBackground.Size = new System.Drawing.Size(49, 17);
            this.chkBackground.TabIndex = 3;
            this.chkBackground.Text = "Фон";
            this.chkBackground.UseVisualStyleBackColor = true;
            this.chkBackground.CheckedChanged += new System.EventHandler(this.chkBackground_CheckedChanged);
            // 
            // stateMapEditView1
            // 
            this.stateMapEditView1.BackColor = System.Drawing.Color.White;
            this.stateMapEditView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateMapEditView1.Location = new System.Drawing.Point(0, 0);
            this.stateMapEditView1.Map = null;
            this.stateMapEditView1.Name = "stateMapEditView1";
            this.stateMapEditView1.Size = new System.Drawing.Size(709, 774);
            this.stateMapEditView1.TabIndex = 3;
            this.stateMapEditView1.Load += new System.EventHandler(this.stateMapEditView1_Load);
            this.stateMapEditView1.Click += new System.EventHandler(this.stateMapEditView1_Click);
            this.stateMapEditView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.stateMapEditView1_MouseMove);
            // 
            // StateMapEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 796);
            this.Controls.Add(this.stateMapEditView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "StateMapEditor";
            this.Text = "Диаграмма состояний";
            this.Load += new System.EventHandler(this.StateMapEditor_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minC)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minT)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox lstStruct;
        private IgorLib.FilePicker backgroundPicker;
        private System.Windows.Forms.CheckBox chkBackground;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbX;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListBox lstPhases;
        private StateMapEditView stateMapEditView1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown maxC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown minC;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown maxT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown minT;
        private System.Windows.Forms.ToolStripStatusLabel btnFinish;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel nowInput;
        private System.Windows.Forms.TextBox tbName;
    }
}