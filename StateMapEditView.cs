﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Solidus
{
    public partial class StateMapEditView : UserControl
    {
        private StateMap _map;
        public StateMap Map
        {
            get
            {
                return _map;
            }
            set
            {
                _map = value;
                this.Invalidate();
            }
        }
        public StateMapEditView()
        {
            InitializeComponent();
        }

        public Image Backdrop;

        private bool _editing;
        public bool IsEditing
        {
            get { return _editing;  }
        }

        private float _vScale;
        private float _hScale;
        private float minX, minY, maxX, maxY;
        public float curMouseX, curMouseY;

        private StateRegion editedRegion;
        private GraphicsPath neuPath;
        public void BeginEditing(StateRegion region)
        {
            if (_editing)
            {
                throw new InvalidOperationException("Already editing");
            }
            _editing = true;
            firstPointExists = false;
            neuPath = new GraphicsPath();
            editedRegion = region;
        }
        public void EndEditing()
        {
            if (!_editing) throw new InvalidOperationException("Not editing yet");
            _editing = false;
            neuPath.CloseFigure();
            editedRegion.MappedRegion = new Region(neuPath);
        }
        private bool firstPointExists;
        private PointF lastPoint;
        public void AddPointToEditedRegion(PointF point)
        {
            if (!_editing) throw new InvalidOperationException("Not editing yet");
            if (!firstPointExists)
            {
                lastPoint = point;
                firstPointExists = true;
            }
            else
            {
                neuPath.AddLine(lastPoint, point);
                lastPoint = point;
                this.Invalidate();
            }
        }
        public void AddScreenPointToEditedRegion(int x, int y)
        {
            AddPointToEditedRegion(new PointF(TranslateXFromScreenToMap(x), TranslateYFromScreenToMap(y)));
        }

        public float TranslateXFromScreenToMap(float x) {
            return Math.Min(Map.MaxConcentration, Math.Max(Map.MinConcentration, Map.MinConcentration + (x - this.Padding.Left) * _hScale));
        }
        public float TranslateYFromScreenToMap(float y)
        {
            return Math.Min(Map.MaxTemperature, Math.Max(Map.MinTemperature, Map.MinTemperature + ((this.Height - this.Padding.Bottom) - (y - this.Padding.Left)) * _vScale)); ;
        }

        public float TranslateXFromMapToScreen(float x)
        {
            return x * (1 / _hScale);
        }
        public float TranslateYFromMapToScreen(float y)
        {
            return y * (1 / _vScale);
        }
        private void StateMapEditView_Load(object sender, EventArgs e)
        {

        }

        private void StateMapEditView_MouseMove(object sender, MouseEventArgs e)
        {
            curMouseX = TranslateXFromScreenToMap(e.X);
            curMouseY = TranslateYFromScreenToMap(e.Y);
            
        }

        private Pen gridPen = Pens.Black;
        private Font axisFont = new Font("Times New Roman", 14.0f);
        private Brush regionBrush = new SolidBrush(Color.FromArgb(125,Color.Red));
        private Brush hiliteRegionBrush = new SolidBrush(Color.FromArgb(125, Color.Gray));
        public StateRegion HighlightRegion;
        private Brush hilitedBrush = new SolidBrush(Color.FromArgb(125, Color.Black));
        private void StateMapEditView_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = this.CreateGraphics();

            g.Clear(this.BackColor);

            if (_map == null) return;

            maxY = (float)(Convert.ToDouble(this.Height - this.Padding.Bottom));
            minY = (float)(Convert.ToDouble(this.Padding.Top));
            minX = (float)(Convert.ToDouble(this.Padding.Left));
            maxX = (float)(Convert.ToDouble(this.Width - this.Padding.Right));
            _hScale = (float)(Map.MaxConcentration - Map.MinConcentration) / ((maxX) - (minX));
            _vScale = (float)(Map.MaxTemperature - Map.MinTemperature) / ((maxY) - (minY));

            if (Backdrop != null)
            {
                g.DrawImage(Backdrop, minX, minY, maxX - minX, maxY - minY);
            }

            // Draw Regions
            GraphicsContainer containerState = g.BeginContainer();
            Matrix upscaler = new Matrix();

            upscaler.Scale(1 / _hScale, -1 / _vScale);
           upscaler.Translate(- (Map.MinConcentration), - (Map.MaxTemperature));

           StringFormat sf = new StringFormat();
           sf.LineAlignment = StringAlignment.Center;
           sf.Alignment = StringAlignment.Center;

            foreach (StateRegion st in Map.Regions)
            {
                if (_editing)
                {
                    if (st == editedRegion)
                    {
                        continue;
                    }
                }
                Region drawable = st.MappedRegion.Clone();

                drawable.Transform(upscaler);
              
                g.FillRegion(st == HighlightRegion ? hilitedBrush : new SolidBrush(Color.FromArgb(128,st.State.DrawingColor)), drawable);
                g.DrawString(st.State.UserReadableName, axisFont, hiliteRegionBrush, drawable.GetBounds(g),sf);
              //  g.DrawRectangle(Pens.LightGoldenrodYellow, Rectangle.Round(drawable.GetBounds(g)));
            }

            // Draw edited region
            if (_editing)
            {
                Region drawable = editedRegion.MappedRegion.Clone();
                drawable.Transform(upscaler);
                g.FillRegion(hiliteRegionBrush, drawable);

                GraphicsPath p = (GraphicsPath)neuPath.Clone();
                p.Transform(upscaler);
                g.DrawPath(Pens.Green, p);

                g.DrawEllipse(Pens.OrangeRed, TranslateXFromMapToScreen(lastPoint.X), TranslateYFromMapToScreen(lastPoint.Y), 1.5f, 1.5f);
            }

            g.EndContainer(containerState);
            
            // Draw the axes
            g.DrawLine(gridPen, minX, maxY, maxX, maxY); // X axis
            g.DrawLine(gridPen, minX, minY, minX, maxY); // Y axis
            
            // Axis labels
            SizeF temp;
            temp = g.MeasureString(Map.MaxTemperature.ToString(), axisFont);
            g.DrawString(Map.MaxTemperature.ToString(), axisFont, Brushes.Black, new RectangleF(minX, minY, temp.Width,temp.Height));
            temp = g.MeasureString(Map.MinTemperature.ToString(), axisFont);
            g.DrawString(Map.MinTemperature.ToString(), axisFont, Brushes.Black, new RectangleF(minX, maxY- temp.Height, temp.Width, temp.Height));
            temp = g.MeasureString(Map.MaxConcentration.ToString(), axisFont);
            g.DrawString(Map.MaxConcentration.ToString(), axisFont, Brushes.Black, new RectangleF(maxX, maxY, temp.Width, temp.Height));
            temp = g.MeasureString(Map.MinConcentration.ToString(), axisFont);
            g.DrawString(Map.MinConcentration.ToString(), axisFont, Brushes.Black, new RectangleF(minX, maxY, temp.Width, temp.Height));


            // Draw pointer
            if (IsEditing)
            {
                g.FillEllipse(Brushes.DarkRed, TranslateXFromMapToScreen(curMouseX)-0.5f, TranslateYFromMapToScreen(curMouseY) - 0.5f, 1.0f, 1.0f);
            }
        }
    }
}
