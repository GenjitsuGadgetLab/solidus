﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Solidus
{
    public partial class StateMapEditor : Form
    {
        StateMap editedMap;
        public StateMapEditor(StateMap map)
        {
            InitializeComponent();
            editedMap = map;
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        void relistRegions()
        {
            lstStruct.Items.Clear();
            foreach (StateRegion r in editedMap.Regions)
            {
                lstStruct.Items.Add(r.State.UserReadableName);
            }
            stateMapEditView1.Invalidate();
        }
        void relistPhases()
        {
            lstPhases.Items.Clear();
            foreach (Phase p in editedMap.KnownPhases)
            {
                lstPhases.Items.Add(p.UserReadableName);
            }
        }
        private void StateMapEditor_Load(object sender, EventArgs e)
        {
            stateMapEditView1.Map = editedMap;
            maxC.Value = (decimal)editedMap.MaxConcentration;
            minT.Value = (decimal)editedMap.MinTemperature;
            minC.Value = (decimal)editedMap.MinConcentration;
            maxT.Value = (decimal)editedMap.MaxTemperature;
            tbName.Text = editedMap.Name;
            relistPhases();
            relistRegions();
        }
        void updateBackdrop()
        {
            if (chkBackground.Checked && backgroundPicker.SelectedPath.Length > 0)
            {
                stateMapEditView1.Backdrop = new Bitmap(backgroundPicker.SelectedPath);
                stateMapEditView1.Invalidate();
            }
            else
            {
                stateMapEditView1.Backdrop = null;
                stateMapEditView1.Invalidate();
            }
        }
        private void chkBackground_CheckedChanged(object sender, EventArgs e)
        {
            updateBackdrop();
        }

        private void backgroundPicker_SelectionChanged(IgorLib.FilePicker sender, string newPath)
        {
            updateBackdrop();
        }

        private void stateMapEditView1_Load(object sender, EventArgs e)
        {
            
        }

        private void stateMapEditView1_DoubleClick(object sender, EventArgs e)
        {

            editStruct();
        }

        private void stateMapEditView1_Click(object sender, EventArgs e)
        {
            
            if (stateMapEditView1.IsEditing)
            {
                // beginEdit
                lblStatus.Text = String.Format("Ввод точки {0}% {1}degC", stateMapEditView1.curMouseX, stateMapEditView1.curMouseY);
                stateMapEditView1.AddPointToEditedRegion(new PointF(stateMapEditView1.curMouseX, stateMapEditView1.curMouseY));
            }
            else
            {
                foreach (StateRegion region in editedMap.Regions)
                {
                    if (region.MappedRegion.IsVisible(new PointF(stateMapEditView1.curMouseX, stateMapEditView1.curMouseY)))
                    {
                        lstStruct.SelectedIndex = editedMap.Regions.IndexOf(region);
                        break;
                    }
                    else lstStruct.SelectedIndex = -1;
                }
                lblStatus.Text = "Не включен режим редактирования";
            }
            stateMapEditView1.Invalidate();
            stateMapEditView1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Phase p = new Phase();
            PhaseEditor pe = new PhaseEditor(p, editedMap);
            pe.ShowDialog();
            if (p.UserReadableName.Length > 0 && !lstPhases.Items.Contains(p.UserReadableName))
            {
                editedMap.KnownPhases.Add(p);
                lstPhases.Items.Add(p.UserReadableName);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstPhases.SelectedIndex > 0)
            {
                if (editedMap.PhaseIsUsed(editedMap.KnownPhases[lstPhases.SelectedIndex]))
                {
                   MessageBox.Show(String.Format("Фаза {0} используется!", lstPhases.SelectedItem.ToString()), "Ой",MessageBoxButtons.OK,MessageBoxIcon.Error);
                   return;
                }
                editedMap.KnownPhases.RemoveAt(lstPhases.SelectedIndex);
                lstPhases.Items.RemoveAt(lstPhases.SelectedIndex);
            }
        }

        private void stateMapEditView1_MouseMove(object sender, MouseEventArgs e)
        {
            lblStatus.Text = String.Format("{0}% {1}degC", stateMapEditView1.curMouseX, stateMapEditView1.curMouseY);
            if (stateMapEditView1.IsEditing)
            {

                tbX.Text = stateMapEditView1.curMouseX.ToString();
                tbY.Text = stateMapEditView1.curMouseY.ToString();
            }
            else
            {
                foreach (StateRegion region in editedMap.Regions)
                {
                    if (region.MappedRegion.IsVisible(new PointF(stateMapEditView1.curMouseX, stateMapEditView1.curMouseY)))
                    {
                        nowInput.Text = region.State.UserReadableName;
                        return;
                    }
                }
                nowInput.Text = "Жидкость";
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lstStruct.SelectedIndex > -1)
            {
                lblStatus.Text = editedMap.Regions[lstStruct.SelectedIndex].State.UserReadableName + " удалено";
                
                editedMap.Regions.RemoveAt(lstStruct.SelectedIndex);
                lstStruct.Items.RemoveAt(lstStruct.SelectedIndex);
                stateMapEditView1.Invalidate();
            }
        }

        void addStruct()
        {
            editedMap.Regions.Add(new StateRegion(new Structure(), new Region()));
            StructureEditor edt = new StructureEditor(editedMap, editedMap.Regions.Last().State);
            edt.hideFinish = true;
            edt.ShowDialog();
            beginEditing(editedMap.Regions.Last());
            relistRegions();
        }

        private StateRegion editedRegion;
        void beginEditing(StateRegion region)
        {
            if (stateMapEditView1.IsEditing)
            {
                throw new InvalidOperationException("Already editing");
            }
            groupBox2.Enabled = groupBox3.Enabled = groupBox5.Enabled = groupBox6.Enabled = false;
            editedRegion = region;
            nowInput.Text = editedRegion.State.UserReadableName;
            stateMapEditView1.BeginEditing(editedRegion);
            //  add the removed region back to liquid
            //editedMap.Regions.First().MappedRegion.Union(editedRegion.MappedRegion);
            btnFinish.Visible = true;
            
        }



        private void button4_Click(object sender, EventArgs e)
        {
            if (stateMapEditView1.IsEditing)
            {
                stateMapEditView1.AddPointToEditedRegion(new PointF((float)Convert.ToDouble(tbX.Text), (float)Convert.ToDouble(tbY.Text)));
            }
            else
            {
                if (MessageBox.Show("Режим ввода не начат. Начать ввод структуры?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    addStruct();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            addStruct();
        }

        private void lstStruct_DoubleClick(object sender, EventArgs e)
        {
            editStruct();
        }

        void editStruct()
        {
            if (lstStruct.SelectedIndex > -1)
            {
                StructureEditor edt = new StructureEditor(editedMap, editedMap.Regions[lstStruct.SelectedIndex].State);
                if (edt.ShowDialog() == DialogResult.Retry)
                {
                    // this means we need to change the region, enter region edit mode here
                    beginEditing(editedMap.Regions[lstStruct.SelectedIndex]);
                }
                relistRegions();
            }
        }

        void endEditing()
        {
            if (!stateMapEditView1.IsEditing)
            {
                throw new InvalidOperationException("Not yet editing");
            }
            stateMapEditView1.EndEditing();
            // substract new region from all affected
            foreach (StateRegion r in editedMap.Regions)
            {
                if (r != editedRegion)
                {
                    r.MappedRegion.Exclude(editedRegion.MappedRegion);
                }
            }
            groupBox2.Enabled = groupBox3.Enabled = groupBox5.Enabled = groupBox6.Enabled = true;
            btnFinish.Visible = false;
            nowInput.Text = "";
            stateMapEditView1.Invalidate();
            stateMapEditView1.Refresh();
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            endEditing();
        }

        private void lstStruct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstStruct.SelectedIndex < 0 || lstStruct.SelectedIndex >= editedMap.Regions.Count) stateMapEditView1.HighlightRegion = null;
            else stateMapEditView1.HighlightRegion = editedMap.Regions[lstStruct.SelectedIndex];
            stateMapEditView1.Invalidate();
        }

        private void lstPhases_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstPhases_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void lstPhases_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lstPhases.SelectedIndex <= 0 || lstPhases.SelectedIndex >= editedMap.KnownPhases.Count) return;
            PhaseEditor pe = new PhaseEditor(editedMap.KnownPhases[lstPhases.SelectedIndex], editedMap);
            pe.ShowDialog();
            relistPhases();
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            editedMap.Name = tbName.Text;
        }

        private void minT_ValueChanged(object sender, EventArgs e)
        {
            editedMap.MinTemperature = (float)minT.Value;
            stateMapEditView1.Invalidate();
        }

        private void maxT_ValueChanged(object sender, EventArgs e)
        {
            editedMap.MaxTemperature = (float)maxT.Value;
            stateMapEditView1.Invalidate();
        }

        private void minC_ValueChanged(object sender, EventArgs e)
        {
            editedMap.MinConcentration = (float)minC.Value;
            stateMapEditView1.Invalidate();
        }

        private void maxC_ValueChanged(object sender, EventArgs e)
        {
            editedMap.MaxConcentration = (float)maxC.Value;
            stateMapEditView1.Invalidate();
        }

        

      
    }
}
