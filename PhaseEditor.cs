﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Solidus
{
    public partial class PhaseEditor : Form
    {
        private Phase editedPhase;
        private StateMap editedMap;
        public PhaseEditor(Phase toEdit, StateMap onMap)
        {
            InitializeComponent();
            editedPhase = toEdit;
            editedMap = onMap;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            editedPhase.TransformsTo.Clear();
            foreach (Phase p in editedMap.KnownPhases)
            {
                if (lstTransformsTo.CheckedItems.Contains(p.UserReadableName))
                {
                    editedPhase.TransformsTo.Add(p);
                }
            }
            if (lstTransformsTo.CheckedItems.Contains(lstTransformsTo.Items[lstTransformsTo.Items.Count-1]))
            {
                editedPhase.TransformsTo.Add(AnyPhase.Any);
            }
            this.Close();
        }

        private void PhaseEditor_Load(object sender, EventArgs e)
        {
            tbName.Text = editedPhase.UserReadableName;
            bool skip = false;
            foreach (Phase p in editedMap.KnownPhases)
            {
                if (!skip) skip = true;
                else if(p != editedPhase) lstTransformsTo.Items.Add(p.UserReadableName);
            }
            foreach (Phase p in editedPhase.TransformsTo)
            {
                lstTransformsTo.SetItemChecked(lstTransformsTo.Items.IndexOf(p.UserReadableName), true);
            }
            lstTransformsTo.Items.Add("любую фазу");
            lblAbbr.Text = editedPhase.UserReadableAbbreviation;
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            editedPhase.UserReadableName = tbName.Text;
            lblAbbr.Text = editedPhase.UserReadableAbbreviation;
        }

    }
}
