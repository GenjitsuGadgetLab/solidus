﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
namespace Solidus
{
    public enum DrawStructureAs
    {
        DoNotDraw,
        DrawAsCrystals,
        DrawOnEdgeOfCrystals,
        FillArea
    }

    public struct StructureDisplayRepresentation
    {
        DrawStructureAs DrawAs;
        Pen OutlinePen;
        Brush FillBrush;
    }

    [XmlType("AnyPhase")]
    public sealed class AnyPhase : Phase
    {
        public static AnyPhase Any = new AnyPhase();
        public AnyPhase() { }
        
    }
    
    public class Reaction
    {
        public Reaction() { }
        Structure from, to;
        public Reaction(Structure reactFrom, Structure reactTo)
        {
            from = reactFrom;
            to = reactTo;
            React();
        }
        public void React()
        {
            foreach (Phase p in from.Phases)
            {
                if (p.TransformsTo.Contains(AnyPhase.Any) && !Sources.Contains(p))
                {
                    Sources.Add(p);
                    break;
                }
                else
                {
                    foreach (Phase pp in p.TransformsTo)
                    {
                        if (to.Phases.Contains(pp) && !Sources.Contains(p))
                        {
                            Sources.Add(p);
                        }
                    }
                }
            }
            foreach (Phase p in to.Phases)
            {
                if (!Sources.Contains(p) && !from.Phases.Contains(p))
                    Products.Add(p);
                
            }
        }
        public List<Phase> Sources = new List<Phase>();
        public List<Phase> Products = new List<Phase>();
        public int C
        {
            get
            {
                List<Phase> ph = new List<Phase>();
                foreach (Phase p in from.Phases)
                    if (!ph.Contains(p))
                        ph.Add(p);
                foreach (Phase p in to.Phases)
                    if (!ph.Contains(p))
                        ph.Add(p);
                return 3 - Products.Count - Sources.Count - ph.Count+2;
            }
        }
    }

    [XmlType("Phase")]
    public class Phase
    {
        [XmlAttribute("Name")]
        public string UserReadableName = "";
        [XmlArray("TransTo")]
        public List<Phase> TransformsTo = new List<Phase>();
        [XmlIgnore]
        public string UserReadableAbbreviation
        {
            get
            {
                if (this.UserReadableName.Length < 1) return "";
                Regex regexObj = new Regex(@"[^\d]");
                return this.UserReadableName.Substring(0,1)+regexObj.Replace(this.UserReadableName, "");
            }
        }
        public Phase() { }
        public Phase(string Name)
        {
            UserReadableName = Name;
        }
    }

    [XmlType("Structure")]
    public class Structure
    {
        [XmlArray("Phases")]
        public List<Phase> Phases = new List<Phase>();
        [XmlAttribute("Marking")]
        public string UserReadableMarking;
        [XmlIgnore]
        public StructureDisplayRepresentation StructureDisplay;
        [XmlIgnore]
        public Color DrawingColor = StateMap.GetRandomColor();
        [XmlAttribute("Color")]
        public string SerializedColor
        {
            get
            {
                return ColorTranslator.ToHtml(DrawingColor);
            }
            set
            {
                DrawingColor = ColorTranslator.FromHtml(value);
            }
        }
        [XmlIgnore]
        public string UserReadableAbbreviation
        {
            get
            {
                Regex regexObj = new Regex(@"[^\d]");
                StringBuilder sb = new StringBuilder();
                if (this.UserReadableMarking != null && this.UserReadableMarking.Length > 0)
                {
                    sb.Append(this.UserReadableMarking.Substring(0, 1));
                    sb.Append(" (");
                }
                bool f = false;
                foreach (Phase p in Phases)
                {
                    if (f) sb.Append(" + ");
                    sb.Append(p.UserReadableAbbreviation);
                    f = true;
                }
                if (this.UserReadableMarking != null && this.UserReadableMarking.Length > 0)
                {
                    sb.Append(")");
                }
                return sb.ToString();
            }

        }
        public string UserReadableName
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                bool f = false;
                if (this.UserReadableMarking != null && this.UserReadableMarking.Length > 0)
                {
                    sb.Append(this.UserReadableMarking);
                    sb.Append(" (");
                }
                foreach (Phase p in Phases)
                {
                    if (f) sb.Append(" + ");
                    sb.Append(p.UserReadableName);
                    f = true;
                }
                if (this.UserReadableMarking != null && this.UserReadableMarking.Length > 0)
                {
                    sb.Append(")");
                }
                return sb.ToString();
            }
        }
       
        public string ReactionWithStructure(Structure otherOne)
        {
            StringBuilder sb = new StringBuilder();
            bool aflag = false;
            Reaction R = new Reaction(this, otherOne);
          
            foreach (Phase p in R.Sources)
            {
                if (sb.Length > 0) sb.Append("+");
                sb.Append(p.UserReadableAbbreviation);
                aflag = true;
            }
            bool bflag = false;
            foreach (Phase p in R.Products)
            {
                if (aflag && !bflag)
                {
                    sb.Append(" → ");
                    aflag = false;
                    bflag = true;
                }
                if (sb.Length > 0 && aflag) sb.Append("+");
                sb.Append(p.UserReadableAbbreviation);
                aflag = true;
            }
            return sb.ToString();
        }
        public int CforReationWithStructure(Structure otherOne)
        {
            Reaction R = new Reaction(this, otherOne);
            return R.C;
        }
        public Structure() { }
        public Structure(Phase phase)
        {
            Phases.Add(phase);
        }
        public Structure(List<Phase> phases, string marking)
        {
            Phases = phases;
            UserReadableMarking = marking;
        }
    }

    [XmlType("StateRegion")]
    public class StateRegion
    {
        [XmlIgnore]
        public Region MappedRegion;
        [XmlElement("Region")]
        public byte[] SerializedRegion
        {
            get
            {
                return MappedRegion.GetRegionData().Data;
            }
            set
            {
                RegionData temp = (new Region()).GetRegionData(); // weird constructor i know
                temp.Data = value;
                MappedRegion = new Region(temp);
            }
        }
        [XmlElement("State")]
        public Structure State;
       public StateRegion(Structure state, Region map)
        {
            MappedRegion = map;
            State = state;
        }
       public StateRegion() { }
    }

    [XmlRoot("StateMap")]
    public class StateMap
    {
        [XmlIgnore]
        private static readonly Random random = new Random();
        public static Color GetRandomColor()
        {
            return Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
        }  
        [XmlAttribute("MinT")]
        public float MinTemperature = 500.0f;
         [XmlAttribute("MaxT")]
        public float MaxTemperature = 1600.0f;
         [XmlAttribute("MaxC")]
        public float MaxConcentration = 6.67f;
         [XmlAttribute("MinC")]
        public float MinConcentration = 0.0001f;
        [XmlArray("Regions")]
        public List<StateRegion> Regions;
        [XmlArray("Phases")]
        public List<Phase> KnownPhases = new List<Phase>();
        [XmlAttribute("Name")]
        public string Name
        {
            get;
            set;
        }
        public bool PhaseIsUsed(Phase phase)
        {
            foreach (StateRegion r in Regions)
            {
                foreach (Phase p in r.State.Phases)
                {
                    if (p.UserReadableName.Equals(phase.UserReadableName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /*public List<StateRegion> HitRegionsOnConcentration(float Concentration)
        {
            if (Concentration < MinConcentration || Concentration > MaxConcentration)
            {
                throw new ArgumentOutOfRangeException("Неверное значение концентрации");
            }

            // TODO!

            return Regions;
        }*/
        public StateMap(string mapName)
        {
            Name = mapName;
            Regions = new List<StateRegion>();
            IsSaved = true;
            KnownPhases.Add(new Phase("Жидкость"));
            KnownPhases[0].TransformsTo.Add(AnyPhase.Any);
          //  Regions.Add(new StateRegion(new Structure(KnownPhases[0]), new Region(new RectangleF(MinConcentration, MinTemperature, (MaxConcentration - MinConcentration), (MaxTemperature - MinTemperature)))));
        }
        public StateMap()
        {
            
        }

        private string _diskPath="";
        [XmlIgnore]
        public  string DiskPath { get { return _diskPath; } }
        [XmlIgnore]
        public  bool IsOnDisk { get { return (_diskPath.Length > 0); } }
        [XmlIgnore]
        public bool IsSaved { get; set; }
        public static Type[] possibleTypes = new Type[] { typeof(StateMap), typeof(StateRegion), typeof(Structure), typeof(Phase), typeof(AnyPhase) };
        public void SaveAs(string path)
        {
            StringWriter stringWriter = new StringWriter();
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            XmlSerializer x = new XmlSerializer(this.GetType(), possibleTypes);

            writerSettings.OmitXmlDeclaration = true;
            writerSettings.Indent = true;

            using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, writerSettings))
            {
                x.Serialize(xmlWriter, this, ns);
            }
            string dummy = stringWriter.ToString();

            StreamWriter w = File.CreateText(path);
            w.Write(dummy);
            w.Close();

            IsSaved = true;
            _diskPath = path;
        }

        public void Save()
        {
            SaveAs(_diskPath);
        }

        public static StateMap ReadFromFile(string filePath)
        {
            StreamReader f = File.OpenText(filePath);
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            XmlSerializer x = new XmlSerializer((new StateMap()).GetType(), possibleTypes);
            StateMap d;
            using (XmlReader xmlReader = XmlReader.Create(f))
            {
                d = (StateMap)x.Deserialize(xmlReader);
            }
            d.IsSaved = true;
            d._diskPath = filePath;
            f.Close();
            return d;
        }
        public enum PredefinedMapKind
        {
            FeFe3CStateMapKind
        }
        public StateMap(PredefinedMapKind PredefKind )  {
            switch (PredefKind)
            {
                case PredefinedMapKind.FeFe3CStateMapKind:
                #region Fe3C Map
                    Name = "Fe-Fe3C";
            Regions = new List<StateRegion>();
            IsSaved = true;
            KnownPhases.Add(new Phase("Жидкость"));
            KnownPhases[0].TransformsTo.Add(AnyPhase.Any);

            KnownPhases.Add(new Phase("Аустенит"));
            KnownPhases.Add(new Phase("Цементит"));
            KnownPhases.Add(new Phase("Цементит 1"));
            KnownPhases.Add(new Phase("Цементит 2"));
            KnownPhases.Add(new Phase("Феррит"));
            KnownPhases.Add(new Phase("Цементит 3"));

            KnownPhases[1].TransformsTo.Add(KnownPhases[5]); // А - Ф
            KnownPhases[1].TransformsTo.Add(KnownPhases[4]); // А - Ц2
            KnownPhases[1].TransformsTo.Add(KnownPhases[6]); // А - Ц3

            // Ж+А
            {
                Structure za = new Structure(new List<Phase>() { KnownPhases[0], KnownPhases[1] }, "");
                GraphicsPath gp = new GraphicsPath();
                gp.AddLine(0.0f, 1493.0f, 2.14f, 1147.0f);
                gp.AddLine(2.14f, 1147.0f, 4.3f, 1147.0f);
                gp.AddLine(4.3f, 1147.0f, 0.0f, 1493.0f);
                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(za, new Region(gp));

                Regions.Add(sr);
            }

            // Ж+Ц1
            {
                Structure zc1 = new Structure(new List<Phase>() { KnownPhases[0], KnownPhases[3] }, "");
                GraphicsPath gp = new GraphicsPath();
                gp.AddLine(4.3f, 1147.0f, 6.67f, 1147.0f);
                gp.AddLine(6.67f, 1147.0f, 6.67f, 1252.0f);
                gp.AddLine(6.67f, 1252.0f, 4.3f, 1147.0f);
                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(zc1, new Region(gp));

                Regions.Add(sr);
            }

            // А
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[1] }, "");
                GraphicsPath gp = new GraphicsPath();
                gp.AddLine(0.0f, 1493.0f, 0.0f, 911.0f);
                gp.AddLine(0.0f, 911.0f, 0.8f, 727.0f);
                gp.AddLine(0.8f, 727.0f, 2.14f, 1147.0f);
                gp.AddLine(2.14f, 1147.0f, 0.0f, 1493.0f);
                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // А+Ц2
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[1], KnownPhases[4] }, "");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(2.14f, 1147.0f, 0.8f, 727.0f);
                gp.AddLine(0.8f, 727.0f, 2.14f, 727.0f);
                gp.AddLine(2.14f, 727.0f, 2.14f, 1147.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // А+Ц2+Л
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[1], KnownPhases[4] }, "А+Ц2+Л");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(2.14f, 1147.0f, 4.3f, 1147.0f);
                gp.AddLine(4.3f, 1147.0f, 4.3f, 727.0f);
                gp.AddLine(4.3f, 727.0f, 2.14f, 727.0f);
                gp.AddLine(2.14f, 727.0f, 2.14f, 1147.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // Ц1+Л
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[1], KnownPhases[3] }, "Ц1+Л");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(4.3f, 727.0f, 4.3f, 1147.0f);
                gp.AddLine(4.3f, 1147.0f, 6.67f, 1147.0f);
                gp.AddLine(6.67f, 1147.0f, 6.67f, 727.0f);
                gp.AddLine(6.67f, 727.0f, 4.3f, 727.0f);
               
                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // Ф+А
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[1],  KnownPhases[5] }, "");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(0.0f, 911.0f, 0.8f, 727.0f);
                gp.AddLine(0.8f, 727.0f, 0.45f, 727.0f);
                gp.AddLine(0.45f, 727.0f, 0.0f, 911.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // Ф
            {
                Structure a = new Structure(new List<Phase>() {  KnownPhases[5] }, "");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(0.0f, 911.0f, 0.45f, 727.0f);
                gp.AddLine(0.45f, 727.0f, 0.05f, 500.0f);
                gp.AddLine(0.05f, 500.0f, 0.0f, 500.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // Ф+Ц3
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[5], KnownPhases[6] }, "");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(0.45f, 727.0f, 0.45f, 500.0f);
                gp.AddLine(0.45f, 500.0f, 0.05f, 500.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // Ф+П
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[5], KnownPhases[2] }, "Ф+П");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(0.45f, 727.0f, 0.45f, 500.0f);
                gp.AddLine(0.45f, 500.0f, 0.8f, 500.0f);
                gp.AddLine(0.8f, 500.0f, 0.8f, 727.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }
            // П+Ц2
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[5], KnownPhases[4] }, "П+Ц2");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(0.8f, 500.0f, 0.8f, 727.0f);
                gp.AddLine(0.8f, 727.0f, 2.14f, 727.0f);
                gp.AddLine(2.14f, 727.0f, 2.14f, 500.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // П+Ц2+Л
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[5], KnownPhases[4], KnownPhases[1] }, "П+Ц2+Л");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(2.14f, 727.0f, 2.14f, 500.0f);
                gp.AddLine(2.14f, 500.0f, 4.3f, 500.0f);
                gp.AddLine(4.3f, 500.0f, 4.3f, 727.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }

            // Ц1+Л
            {
                Structure a = new Structure(new List<Phase>() { KnownPhases[1], KnownPhases[3] }, "Ц1+Л");
                GraphicsPath gp = new GraphicsPath();

                gp.AddLine(4.3f, 727.0f, 4.3f, 500.0f);
                gp.AddLine(4.3f, 500.0f, 6.67f, 500.0f);
                gp.AddLine(6.67f, 500.0f, 6.67f, 727.0f);
                gp.AddLine(6.67f, 727.0f, 4.3f, 727.0f);

                gp.CloseAllFigures();
                StateRegion sr = new StateRegion(a, new Region(gp));

                Regions.Add(sr);
            }
                #endregion
                 break;
            }
        }
    }

}
