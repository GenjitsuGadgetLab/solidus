﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Solidus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        StateMap currentMap = new StateMap("Новая");
        private void редакторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentMap.IsSaved = false;
            StateMapEditor editor = new StateMapEditor(currentMap);
            editor.ShowDialog();
            numericUpDown1.Maximum = (decimal)currentMap.MaxConcentration;
            numericUpDown1.Minimum = (decimal)currentMap.MinConcentration;
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            numericUpDown1.Maximum = (decimal)currentMap.MaxConcentration;
            numericUpDown1.Minimum = (decimal)currentMap.MinConcentration;
        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            lstTransitions.Items.Clear();

            float[] dashValues = { 5, 2, 15, 4 };
            Pen dashPen = new Pen(Color.FromArgb(64, Color.Black), 1);
            dashPen.DashPattern = dashValues;

            //currentMap.MaxTemperature += 100;
            currentMap.MinTemperature -= 100;

            float hScale = pictureBox1.Width / (currentMap.MaxTemperature - currentMap.MinTemperature);
            float vScale = pictureBox1.Height / (currentMap.MaxTemperature - currentMap.MinTemperature);

            progressBar1.Minimum = Convert.ToInt32(currentMap.MinTemperature);
            progressBar1.Maximum = Convert.ToInt32(currentMap.MaxTemperature);
            progressBar1.Value = progressBar1.Minimum;
            progressBar1.Visible = true;

            Graphics g = pictureBox1.CreateGraphics();

            g.Clear(Color.White);

            if (chkCopyright.Checked)
            {
                string copyright = "Genjitsu Gadget Lab Solidus";
                Font f = new Font("System", 8);
                SizeF temp = g.MeasureString(copyright, f);
                g.DrawString(copyright, f, Brushes.LightGray, pictureBox1.Width - temp.Width - 2, pictureBox1.Height - temp.Height);
            }

            Font fnt = new Font("System", 12);
            Brush fBrush = new SolidBrush(Color.FromArgb(128, Color.Black));

            StateRegion currRegion = new StateRegion(new Structure(currentMap.KnownPhases.First()), null);
            float currTemperature = currentMap.MaxTemperature;
            int c = 2;
            int pc = 2;
            string re = "";
            List<PointF> gpoints = new List<PointF>();

            gpoints.Add(new PointF(0, 0));
            float ly = 0;
            float dt = 0.5f;
            float y = (((currentMap.MaxTemperature - currTemperature)) * vScale) ;
            float x = ((currentMap.MaxTemperature - currTemperature)) * hScale;
            for ( currTemperature = currentMap.MaxTemperature; currTemperature >= currentMap.MinTemperature; currTemperature -= dt)
            {
                if (c == 2) y += dt*vScale;
                else if (c == 1) y += (dt / 2)*vScale;
                x += dt * hScale;
                foreach (StateRegion region in currentMap.Regions)
                {
                    if (region.MappedRegion.IsVisible(new PointF((float)numericUpDown1.Value, currTemperature)))
                    {
                        if (currRegion != region)
                        {
                            


                            pc = c;
                            c = currRegion.State.CforReationWithStructure(region.State);
                            if (pc == 0 && c == 0)
                            {
                                gpoints.Add(new PointF(x-10, y));
                                y = currTemperature * vScale;
                            }
                            gpoints.Add(new PointF(x, y));
                            string degs = String.Format("{0}°C", currTemperature);
                            SizeF ds = g.MeasureString(degs, fnt);

                            g.DrawString(degs, fnt, fBrush, 2, y - ds.Height - 2);
                            g.DrawLine(dashPen, 0, y, pictureBox1.Width, y);
                            g.FillEllipse(Brushes.DarkRed, x - 2, y - 2, 4, 4);
                            re = currRegion.State.ReactionWithStructure(region.State);
                            SizeF rs = g.MeasureString(re, fnt);
                           if(cbLabels.Checked) g.DrawString(re, fnt, Brushes.Black, x,  y - rs.Height/2 );
                           
                            lstTransitions.Items.Add(String.Format("{0}°C: {1} (C={2})", currTemperature, re, c));
                            currRegion = region;
                        }
                    }
                }
                
                progressBar1.Value = progressBar1.Maximum - Convert.ToInt32(currTemperature) + progressBar1.Minimum;
                Application.DoEvents();
            }

            gpoints.Add(new PointF(pictureBox1.Width, (c==0?y:pictureBox1.Height)));

            for (int i = 1; i < gpoints.Count; i++)
            {
                g.DrawLine(Pens.DarkRed,gpoints[i - 1], gpoints[i]);
            }

            string sign = currentMap.Name + " " + numericUpDown1.Value + "%";
            SizeF sas = g.MeasureString(sign, fnt);
            g.DrawString(sign, fnt, Brushes.Black, new PointF(0, pictureBox1.Height-sas.Height));

            progressBar1.Visible = false;
            pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            using (Graphics gp = Graphics.FromImage(pictureBox1.Image))
            {
                // Draw onto the bitmap here
                // ....
                gp.CopyFromScreen(pictureBox1.RectangleToScreen(pictureBox1.ClientRectangle).X, pictureBox1.RectangleToScreen(pictureBox1.ClientRectangle).Y, 0, 0, pictureBox1.Size);
            }
            //currentMap.MaxTemperature -= 100;
            currentMap.MinTemperature += 100;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null) button1.PerformClick();
            SaveFileDialog sf = new SaveFileDialog();
            sf.Filter = "PNG|*.png|BMP|*.bmp|JPG|*.jpg";
            if (sf.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                pictureBox1.Image.Save(sf.FileName);
            }
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private bool SaveMap()
        {
            try
            {
                if (currentMap.IsOnDisk)
                {
                    currentMap.Save();
                    return true;
                }
                else
                {
                    saveFile.FileName = currentMap.Name;
                    if (saveFile.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
                    {
                        currentMap.SaveAs(saveFile.FileName);
                        return true;
                    }
                    else return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Не сохранено: "+e.Message, "Не удалось", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return false;
            }
        }
        private bool AskToSaveIfNeeded()
        {
            if (!currentMap.IsSaved)
            {
                switch (MessageBox.Show("Текущая диаграмма состояний не сохранена. Сохранить?", "Диаграмма состояний", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        if (!SaveMap()) return false;
                        else return true;

                    case System.Windows.Forms.DialogResult.Cancel:
                        return false;

                    default:
                        return true;

                }
            }
            else return true;
        }
        private void загрузитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!AskToSaveIfNeeded()) return;
            if (openFile.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                try
                {
                    currentMap = StateMap.ReadFromFile(openFile.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не загружено: " + ex.Message, "Не удалось", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveMap();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new AboutBox()).ShowDialog();
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!currentMap.IsOnDisk) saveFile.FileName = currentMap.Name;
            else saveFile.FileName = currentMap.DiskPath;
            if (saveFile.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                currentMap.SaveAs(saveFile.FileName);
            }

        }

        private void новаяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!AskToSaveIfNeeded()) return;
            currentMap = new StateMap("Неименованная");
        }

        private void ферритЦементитFeFe3CToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!AskToSaveIfNeeded()) return;
            currentMap = new StateMap(StateMap.PredefinedMapKind.FeFe3CStateMapKind);
        }

    }
}
