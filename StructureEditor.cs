﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Solidus
{
    public partial class StructureEditor : Form
    {
        public bool hideFinish = false;
        public StructureEditor(StateMap map, Structure structure)
        {
            InitializeComponent();
            if (map == null || structure == null)
            {
                throw new ArgumentNullException();
            }
            editedMap = map;
            editedStructure = structure;
        }
        private StateMap editedMap;
        private Structure editedStructure;
        private void StructureEditor_Load(object sender, EventArgs e)
        {
            colPick.BackColor = editedStructure.DrawingColor;
            lstPhases.Items.Clear();
            foreach (Phase p in editedMap.KnownPhases)
            {
                lstPhases.Items.Add(p.UserReadableName);
            }
            tbMarking.Text = editedStructure.UserReadableMarking;
            foreach (Phase p in editedStructure.Phases)
            {
                lstPhases.SetItemChecked(lstPhases.Items.IndexOf(p.UserReadableName), true);
            }
            button1.Visible = !hideFinish;
        }

        void save()
        {
            editedStructure.UserReadableMarking = tbMarking.Text;
            editedStructure.Phases.Clear();
            foreach (Phase p in editedMap.KnownPhases)
            {
                if (lstPhases.CheckedItems.Contains(p.UserReadableName))
                {
                    editedStructure.Phases.Add(p);
                }
            }
            editedMap.IsSaved = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            save();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            save();
            this.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.Close();
        }

        private void colPick_Click(object sender, EventArgs e)
        {
            colorDialog1.Color = editedStructure.DrawingColor;
            if (colorDialog1.ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
            {
                editedStructure.DrawingColor = colorDialog1.Color;
                colPick.BackColor = editedStructure.DrawingColor;
            }
        }
    }
}
